package com.spring.sandbox.responses;

import lombok.Data;

@Data
public class GenericResponse {
    private String message;
    private int httpStatus;
    private Object data;

    public GenericResponse() {
        this.data = null;
        this.httpStatus = 0;
        this.message = "";
    }
}
