package com.spring.sandbox.responses;

import com.spring.sandbox.models.Model;

public class SandBoxResponse {
    public Long userId;
    public String firstName;
    public String lastName;
    public String description;
    public String email;

    public SandBoxResponse(Model model) {
        this.userId = model.getUserId();
        this.firstName = model.getFirstName();
        this.email = model.getEmail();
        this.lastName = model.getLastName();
        this.description = model.getDescription();
    }
}
