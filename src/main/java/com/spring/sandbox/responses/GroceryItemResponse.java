package com.spring.sandbox.responses;

import com.spring.sandbox.models.GroceryItemModel;
import lombok.Getter;

@Getter
public class GroceryItemResponse {
    private final Long groceryItemId;
    private final String name;
    private final int quantity;
    private final String category;

    public GroceryItemResponse(GroceryItemModel groceryItemModel) {
        this.groceryItemId = groceryItemModel.getGroceryItemId();
        this.name = groceryItemModel.getName();
        this.quantity = groceryItemModel.getQuantity();
        this.category = groceryItemModel.getCategory();
    }
}
