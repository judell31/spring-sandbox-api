package com.spring.sandbox.responses;

import com.spring.sandbox.models.Definition;
import com.spring.sandbox.models.WordModel;

import java.util.List;

public class WordResponse {
    public String word;
    public String pronunciation;
    public List<Definition> definitions;
    
    public WordResponse(WordModel wordModel) {
        this.word = wordModel.getWord();
        this.definitions = wordModel.getDefinitions();
        this.pronunciation = wordModel.getPronunciation();
    }
}
