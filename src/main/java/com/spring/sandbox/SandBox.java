package com.spring.sandbox;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = SandBox.class)
@EnableAsync
//@PropertySource({"classpath:"})
public class SandBox {
//    private final static Logger LOGGER = LogManager.getLogger(SandBox.class);
    private final static Logger LOGGER = LoggerFactory.getLogger(SandBox.class);

    public static void main(String[] args) {
//        ConfigurableApplicationContext context = SpringApplication.run(SandBox.class, args);
//        VaultConfiguration vaultConfiguration = context.getBean(VaultConfiguration.class);

        SpringApplication.run(SandBox.class, args);

        LOGGER.info("Spring Sandbox Started");
//        logger.info("Vault DB User: " + vaultConfiguration.getSandboxUser());
//        logger.info("Vault DB Password: " + vaultConfiguration.getSandboxPass());
    }
}
