package com.spring.sandbox.controllers;

import com.spring.sandbox.constants.RouteConstants;
import com.spring.sandbox.requests.GroceryItemRequest;
import com.spring.sandbox.requests.MysqlDeleteUserRequest;
import com.spring.sandbox.requests.Request;
import com.spring.sandbox.requests.SandBoxRequest;
import com.spring.sandbox.responses.*;
import com.spring.sandbox.services.SandBoxService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

//Todo: Move routes to constants
@RestController
@CrossOrigin(originPatterns = "*", allowedHeaders = "*", allowCredentials = "true")
@RequiredArgsConstructor
@Validated
public class Controller {

    @Autowired
    private SandBoxService sandBoxService;

    @GetMapping(RouteConstants.GET_USER)
    public SandBoxResponse get(@PathVariable("userId") Long userId) throws Exception {
        return sandBoxService.getUser(userId);
    }

    @GetMapping(RouteConstants.GET_OWL_WORD)
    public ResponseEntity<WordResponse> getWord(@PathVariable(value = "word")String word) {
        return sandBoxService.getWord(word);
    }

//    @GetMapping("/mysql-users")
//    public List<MysqlUserResponse> getMysqlUsers() {
//        return sandBoxService.getMysqlUsers();
//    }

    @PostMapping(RouteConstants.ADD_USER)
    public SandBoxResponse add(@RequestBody @Valid SandBoxRequest sandBoxRequest) {
        return sandBoxService.addUser(sandBoxRequest);
    }

    @PostMapping(RouteConstants.SAVE_GROCERY_ITEM)
    public GroceryItemResponse saveGroceryItem(@RequestBody GroceryItemRequest groceryItemRequest) {
        return sandBoxService.saveGroceryItem(groceryItemRequest);
    }

    @GetMapping(RouteConstants.GET_GROCERY_ITEMS)
    public List<GroceryItemResponse> getGroceryItems() {
        return sandBoxService.getGroceryItems();
    }

    @DeleteMapping(RouteConstants.DELETE_GROCERY_ITEM)
    public GenericResponse deleteGroceryItems(@PathVariable(value = "itemId") long itemId) {
        return sandBoxService.deleteItem(itemId);
    }

    @PutMapping(RouteConstants.UPDATE_USER)
    public SandBoxResponse updateUser(@PathVariable("userId") Long userId, @RequestBody Request request) {
        return sandBoxService.updateUser(userId, request);
    }

    @DeleteMapping(RouteConstants.DELETE_USER)
    public Integer delete(@PathVariable("userId") Long userId){
        return sandBoxService.deleteUser(userId);
    }

//    @DeleteMapping("/delete-mysql-user")
//    public HttpStatus deleteMysqlUser(@RequestBody MysqlDeleteUserRequest mysqlDeleteUserRequest) {
//        return sandBoxService.deleteMysqlUser(mysqlDeleteUserRequest);
//    }
}
