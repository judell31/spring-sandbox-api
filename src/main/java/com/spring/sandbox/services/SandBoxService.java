package com.spring.sandbox.services;

import com.spring.sandbox.constants.OwlConstants;
import com.spring.sandbox.dao.Dao;
import com.spring.sandbox.dao.GroceryItemDao;
import com.spring.sandbox.exceptions.exceptions.NotFoundException;
import com.spring.sandbox.models.GroceryItemModel;
import com.spring.sandbox.models.Model;
import com.spring.sandbox.models.MysqlUserModel;
import com.spring.sandbox.models.WordModel;
import com.spring.sandbox.requests.GroceryItemRequest;
import com.spring.sandbox.requests.MysqlDeleteUserRequest;
import com.spring.sandbox.requests.Request;
import com.spring.sandbox.requests.SandBoxRequest;
import com.spring.sandbox.responses.GenericResponse;
import com.spring.sandbox.responses.GroceryItemResponse;
import com.spring.sandbox.responses.SandBoxResponse;
import com.spring.sandbox.responses.WordResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SandBoxService {
    @Autowired
    private Dao dao;

    @Autowired
    private GroceryItemDao groceryItemDao;

    private final static Logger LOGGER = LoggerFactory.getLogger(SandBoxService.class);

    public SandBoxResponse getUser(Long userId) throws Exception {
        Optional<Model> model = dao.findByUserId(userId);

        if (model.isEmpty()) {
            throw new NotFoundException("No user was found with that ID.");
        }

        return new SandBoxResponse(model.get());
    }

//    public List<MysqlUserResponse> getMysqlUsers() {
//        List<MysqlUserModel> mysqlUserModels = mysqlDao.findAll();
//        List<MysqlUserResponse> mysqlUserResponses = new ArrayList<>();
//
//        for (MysqlUserModel mysqlUserModel : mysqlUserModels) {
//            mysqlUserResponses.add(new MysqlUserResponse(mysqlUserModel));
//        }
//
//        return mysqlUserResponses;
//    }

    public ResponseEntity<WordResponse> getWord(String word) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<WordModel> httpEntity = new HttpEntity<>(headers);

        headers.set("Authorization", OwlConstants.AUTH_TOKEN);

        try {
            ResponseEntity<WordModel> wordResponse = restTemplate.exchange(
                    OwlConstants.OWL_URL + word,
                    HttpMethod.GET,
                    httpEntity,
                    WordModel.class
            );

            return ResponseEntity.ok(new WordResponse(wordResponse.getBody()));
        } catch (HttpClientErrorException httpClientErrorException) {
            LOGGER.error("That word does not exist");

            throw new NotFoundException("That word does not exist");
        }
    }

    public GroceryItemResponse saveGroceryItem(GroceryItemRequest groceryItemRequest) {
        GroceryItemModel groceryItemModel = new GroceryItemModel();

        groceryItemModel.setCategory(groceryItemRequest.getCategory());
        groceryItemModel.setQuantity(groceryItemRequest.getQuantity());
        groceryItemModel.setName(groceryItemRequest.getName());

        groceryItemDao.save(groceryItemModel);

        return new GroceryItemResponse(groceryItemModel);
    }

    public List<GroceryItemResponse> getGroceryItems() {
        List<GroceryItemModel> groceryItemModelList = groceryItemDao.findAll();
        List<GroceryItemResponse> groceryItemResponseList = new ArrayList<>();

        for (GroceryItemModel groceryItemModel : groceryItemModelList) {
            groceryItemResponseList.add(new GroceryItemResponse(groceryItemModel));
        }

        return groceryItemResponseList;
    }

    public GenericResponse deleteItem(long itemId) {
        GenericResponse genericResponse = new GenericResponse();

        groceryItemDao.deleteById(itemId);

        genericResponse.setMessage("Item deleted");
        genericResponse.setHttpStatus(HttpStatus.OK.value());

        return genericResponse;
    }

    public SandBoxResponse addUser(SandBoxRequest sandBoxRequest) {
        Model model = new Model();

        model.setDescription(sandBoxRequest.getDescription());
        model.setFirstName(sandBoxRequest.getFirstName());
        model.setLastName(sandBoxRequest.getLastName());
        model.setEmail(sandBoxRequest.getEmail());

        dao.save(model);

        LOGGER.info("A user was added.");

        return new SandBoxResponse(model);
    }

    public SandBoxResponse updateUser(Long userId, Request request) {
        Optional<Model> model = dao.findByUserId(userId);
        Model modelRecord = model.get();

        modelRecord.setLastName(request.getLastName());
        modelRecord.setFirstName(request.getFirstName());
        modelRecord.setDescription(request.getDescription());
        dao.save(model.get());

        return new SandBoxResponse(model.get());
    }

    public Integer deleteUser(Long userId) {
        Optional<Model> model = dao.findByUserId(userId);
        dao.delete(model.get());

        return HttpStatus.OK.value();
    }

//    public HttpStatus deleteMysqlUser(MysqlDeleteUserRequest mysqlDeleteUserRequest) {
//        Optional<MysqlUserModel> mysqlUserModel = mysqlDao.findByUser(mysqlDeleteUserRequest.getUser());
//        mysqlDao.delete(mysqlUserModel.get());
//
//        return HttpStatus.OK;
//    }
}
