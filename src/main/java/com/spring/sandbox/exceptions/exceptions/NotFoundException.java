package com.spring.sandbox.exceptions.exceptions;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String errorMsg){
        super(errorMsg);
    }
}
