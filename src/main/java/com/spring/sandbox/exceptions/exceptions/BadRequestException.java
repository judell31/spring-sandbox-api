package com.spring.sandbox.exceptions.exceptions;

public class BadRequestException extends RuntimeException{

    public BadRequestException(String errorMsg){
        super(errorMsg);
    }
}
