package com.spring.sandbox.exceptions.exceptions;

//Todo: this exception does not return json
public class ForbiddenException extends RuntimeException {
    public ForbiddenException(String errorMsg){
        super(errorMsg);
    }
}
