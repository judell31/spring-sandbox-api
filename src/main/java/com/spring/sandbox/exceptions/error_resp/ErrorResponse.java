package com.spring.sandbox.exceptions.error_resp;

import lombok.Data;

@Data
public class ErrorResponse {
    private String timeStamp;
    private Integer status;
    private String error;
    private String message;
    private String path;

    public ErrorResponse(String message, String timeStamp, Integer status, String error, String path){
        this.timeStamp = timeStamp;
        this.status = status;
        this.error = error;
        this.path = path;
        this.message = message;
    }

//    public String getTimestamp() {
//        return timeStamp;
//    }
//
//    public void setTimestamp(String timestamp) {
//        this.timeStamp = timestamp;
//    }
//
//    public Integer getStatus() {
//        return status;
//    }
//
//    public void setStatus(Integer status) {
//        this.status = status;
//    }
//
//    public String getError() {
//        return error;
//    }
//
//    public void setError(String error) {
//        this.error = error;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
//    public String getPath() {
//        return path;
//    }
//
//    public void setPath(String path) {
//        this.path = path;
//    }
}
