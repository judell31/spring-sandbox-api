package com.spring.sandbox.exceptions.advice;

import com.spring.sandbox.exceptions.error_resp.ErrorResponse;
import com.spring.sandbox.exceptions.exceptions.BadRequestException;
import com.spring.sandbox.exceptions.exceptions.ForbiddenException;
import com.spring.sandbox.exceptions.exceptions.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

//Todo: this package needs a refactor
@ControllerAdvice
public class Advice {

    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity<ErrorResponse> handleForbiddenException(ForbiddenException e, WebRequest request){
        HttpStatus httpStatus = HttpStatus.FORBIDDEN;
        return new ResponseEntity<>(getErrorResponse(e.getMessage(), request, httpStatus), new HttpHeaders(), httpStatus);
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException e, WebRequest request){
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(getErrorResponse(e.getMessage(), request, httpStatus), new HttpHeaders(), httpStatus);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponse> handleInvalidException(BadRequestException e, WebRequest request){
        HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(getErrorResponse(e.getMessage(), request, httpStatus), new HttpHeaders(), httpStatus);
    }

    private static ErrorResponse getErrorResponse(String errorMessage, WebRequest request, HttpStatus httpStatus){
        return new ErrorResponse(
                errorMessage,
                LocalDateTime.now().toString(),
                httpStatus.value(),
                httpStatus.getReasonPhrase(),
                ((ServletWebRequest) request).getRequest().getRequestURI()
        );
    }
}
