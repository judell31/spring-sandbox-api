package com.spring.sandbox.dao;

import com.spring.sandbox.models.Model;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface Dao extends JpaRepository<Model, Integer> {

    Optional<Model> findByUserId(Long userId);
}
