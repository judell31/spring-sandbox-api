package com.spring.sandbox.dao;

import com.spring.sandbox.models.GroceryItemModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroceryItemDao extends JpaRepository<GroceryItemModel, Long> {
}
