package com.spring.sandbox.requests;

import lombok.Data;

@Data
public class Request {
    private String firstName;
    private String lastName;
    private String description;
}
