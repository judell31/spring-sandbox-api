package com.spring.sandbox.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

//import javax.validation.constraints.Email;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SandBoxRequest {
    private String firstName;
    //TODO: Get message in response
//    @Email(message = "Invalid email")
    private String email;
    private String lastName;
    private String description;
}
