package com.spring.sandbox.requests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

//import javax.validation.constraints.Email;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroceryItemRequest {
    private String name;
    private int quantity;
    private String category;
}
