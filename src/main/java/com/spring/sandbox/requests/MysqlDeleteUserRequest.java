package com.spring.sandbox.requests;

import lombok.Data;

@Data
public class MysqlDeleteUserRequest {
    private String user;
}
