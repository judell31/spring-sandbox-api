package com.spring.sandbox.constants;

public class OwlConstants {
    //TODO: Move this to vault secret
    public final static String AUTH_TOKEN = "Token 51fd1edd0479b71faea8a096e460b5e687c4e9da";

    public final static String OWL_URL = "https://owlbot.info/api/v4/dictionary/";
}
