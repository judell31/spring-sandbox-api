package com.spring.sandbox.constants;

//TODO: Should this be an enum class?
public class RouteConstants {
    private final static String[] SECURED_ROUTES = {

    };

    private final static String[] PUBLIC_ROUTES = {
            RouteConstants.ADD_USER,
            RouteConstants.GET_USER,
            RouteConstants.SAVE_GROCERY_ITEM
    };

    public final static String ADD_USER = SpringSandBoxConstants.BASE_URL + "add-user";
    public final static String GET_USER = SpringSandBoxConstants.BASE_URL + "get-user/{userId}";
    public final static String UPDATE_USER = SpringSandBoxConstants.BASE_URL + "update-user/{userId}";
    public final static String SAVE_GROCERY_ITEM = SpringSandBoxConstants.BASE_URL + "save-grocery-item";
    public final static String DELETE_GROCERY_ITEM = SpringSandBoxConstants.BASE_URL + "delete-grocery-item/{itemId}";
    public final static String GET_GROCERY_ITEMS = SpringSandBoxConstants.BASE_URL + "get-grocery-items";
    public final static String GET_OWL_WORD = SpringSandBoxConstants.BASE_URL + "get-word/{word}";
    public final static String DELETE_USER = SpringSandBoxConstants.BASE_URL + "delete-user/{userId}";
}
