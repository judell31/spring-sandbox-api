package com.spring.sandbox.services;

import com.spring.sandbox.dao.Dao;
import com.spring.sandbox.exceptions.exceptions.NotFoundException;
import com.spring.sandbox.models.Model;
import com.spring.sandbox.models.WordModel;
import com.spring.sandbox.requests.SandBoxRequest;
import com.spring.sandbox.responses.SandBoxResponse;
import com.spring.sandbox.responses.WordResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class SandBoxServiceTest {
    @InjectMocks
    SandBoxService sandBoxService;

    @Mock
    Dao dao;

    @BeforeEach
    void setup() {
//        this.sandBoxService = new SandBoxService();
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void addUserTestSuccess() {
        Model model = new Model();
        SandBoxRequest sandBoxRequest = new SandBoxRequest();

        model.setDescription("des");
        model.setFirstName("first");
        model.setLastName("last");
        model.setEmail("email");

        sandBoxRequest.setDescription("des");
        sandBoxRequest.setFirstName("first");
        sandBoxRequest.setLastName("last");
        sandBoxRequest.setEmail("email");

        SandBoxResponse expectedResponse = new SandBoxResponse(model);
        SandBoxResponse actualResponse = sandBoxService.addUser(sandBoxRequest);
//
//        //TODO: Not sure if this is needed
//        when(dao.save(any())).thenReturn(model);

        assertThat(actualResponse.description, is(equalTo(expectedResponse.description)));
//        verify(dao, times(1));
//        verifyNoInteractions(dao);
    }

    //TODO: Look at this again
    @Test
    void getWordSuccess() {
        WordModel wordModel = new WordModel();
        WordResponse wordResponse = new WordResponse(wordModel);

        ResponseEntity<WordResponse> expectedResponse = ResponseEntity.status(HttpStatus.OK).body(wordResponse);

        ResponseEntity<WordResponse> actualResponse = sandBoxService.getWord("word");

        assertThat(actualResponse.getStatusCode(), is(equalTo(expectedResponse.getStatusCode())));
    }

    @Test
    void getWordFail() {
        assertThrows(
                NotFoundException.class, () -> sandBoxService.getWord("asdfg")
        );
    }

    @ParameterizedTest
    @CsvSource({"true, false"})
    void paramTest(boolean isTrue, boolean isFalse) {
        // Do stuff here
    }
}
